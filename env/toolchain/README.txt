Preparing to build
-------------------

Required packages for building on Raspbian Buster:

sudo apt-get install autoconf automake bison flex libncurses5-dev \
libreadline-dev texinfo

For building gcc are required:

sudo apt-get install libgmp-dev libmpfr-dev libmpc-dev

Some of the tools for devkitARM also require:

sudo apt-get install libfreeimage-dev libmagick++-dev libfftw3-dev


Building the devkit
--------------------

sudo ./build-devkit.sh
