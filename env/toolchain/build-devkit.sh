#!/bin/bash

#---------------------------------------------------------------------------------
# devkitARM build scripts for Raspberry Pi (Raspbian Buster)
#---------------------------------------------------------------------------------

GCC_VER=10.2.0
BINUTILS_VER=2.34
NEWLIB_VER=3.3.0

GENERAL_TOOLS_VER=1.2.0

LIBGBA_VER=0.5.2
LIBTONC_VER=1.4.3
GBATOOLS_VER=1.2.0

DKARM_RULES_VER=1.2.1
DKARM_CRTLS_VER=1.1.0

LIBNDS_VER=1.8.1
DEFAULT_ARM7_VER=0.7.4
DSWIFI_VER=0.4.2
MAXMOD_VER=1.0.14
FILESYSTEM_VER=0.9.14
LIBFAT_VER=1.2.0

DSTOOLS_VER=1.2.1
GRIT_VER=0.8.16
NDSTOOL_VER=2.1.2
MMUTIL_VER=1.9.0

#---------------------------------------------------------------------------------
function extract_and_patch {
	if [ ! -f extracted-$1-$2 ]
	then
		echo "extracting $1-$2"
		tar -xf "$SRCDIR/$1-$2.tar.$3" || { echo "Error extracting $1"; exit 1; }
		touch extracted-$1-$2
	fi
	if [[ ! -f patched-$1-$2 && -f $patchdir/$1-$2.patch ]]
	then
		echo "patching $1-$2"
		patch -p1 -d $1-$2 -i $patchdir/$1-$2.patch || { echo "Error patching $1"; exit 1; }
		touch patched-$1-$2
	fi
}

#---------------------------------------------------------------------------------
# Set env variables
#---------------------------------------------------------------------------------
export DEVKITPRO=/opt/devkitpro
export DEVKITARM=${DEVKITPRO}/devkitARM

export PATH=$PATH:${DEVKITPRO}/tools/bin

#---------------------------------------------------------------------------------
# Sane defaults for building toolchain
#---------------------------------------------------------------------------------
export CFLAGS="-O2 -pipe"
export CXXFLAGS="$CFLAGS"
unset LDFLAGS

CROSS_PARAMS="-build=armv7l-unknown-linux-gnu"

toolsprefix=$DEVKITPRO/tools
prefix=$DEVKITARM

patchdir=$(pwd)/patches
scriptdir=$(pwd)/scripts

target=arm-none-eabi

archives="binutils-${BINUTILS_VER}.tar.xz
	gcc-${GCC_VER}.tar.xz
	newlib-${NEWLIB_VER}.tar.gz
	devkitarm-rules-${DKARM_RULES_VER}.tar.xz
	devkitarm-crtls-${DKARM_CRTLS_VER}.tar.xz"

hostarchives="general-tools-${GENERAL_TOOLS_VER}.tar.bz2
	gba-tools-${GBATOOLS_VER}.tar.bz2
	grit-${GRIT_VER}.tar.bz2
	dstools-${DSTOOLS_VER}.tar.bz2
	ndstool-${NDSTOOL_VER}.tar.bz2
	mmutil-${MMUTIL_VER}.tar.bz2"

targetarchives="libnds-src-${LIBNDS_VER}.tar.bz2
	libgba-src-${LIBGBA_VER}.tar.bz2
	libtonc-src-${LIBTONC_VER}.tar.bz2
	dswifi-src-${DSWIFI_VER}.tar.bz2
	maxmod-src-${MAXMOD_VER}.tar.bz2
	default-arm7-src-${DEFAULT_ARM7_VER}.tar.bz2
	libfilesystem-src-${FILESYSTEM_VER}.tar.bz2
	libfat-src-${LIBFAT_VER}.tar.bz2"

BUILDSCRIPTDIR=$(pwd)
SRCDIR=$(pwd)/.download
mkdir -p $SRCDIR
cd $SRCDIR

for archive in $archives $hostarchives $targetarchives
do
	echo $archive
	if [ ! -f $archive ]
	then
		wget http://asscore.pl/download/devkitpro/$archive || { echo "Error: Failed to download $archive"; exit 1; }
	fi
done

cd $BUILDSCRIPTDIR
BUILDDIR=$(pwd)/.build
mkdir -p $BUILDDIR
cd $BUILDDIR

extract_and_patch binutils $BINUTILS_VER xz
extract_and_patch gcc $GCC_VER xz
extract_and_patch newlib $NEWLIB_VER gz

for archive in $hostarchives
do
	destdir=$(echo $archive | sed -e 's/\(.*\)\.tar\.bz2/\1/' )
	if [ ! -d $destdir ]
	then
		echo "extracting $archive"
		tar -xjf "$SRCDIR/$archive"
		if [[ ! -f patched-$destdir && -f $patchdir/$destdir.patch ]]
		then
			echo "patching $destdir"
			patch -p1 -d $destdir -i $patchdir/$destdir.patch || { echo "Error patching $destdir"; exit 1; }
			touch patched-$destdir
		fi
	fi
done

for archive in $targetarchives
do
	destdir=$(echo $archive | sed -e 's/\(.*\)-src-\(.*\)\.tar\.bz2/\1-\2/' )
	if [ ! -d $destdir ]
	then
		echo "extracting $archive"
		mkdir -p $destdir
		bzip2 -cd "$SRCDIR/$archive" | tar -xf - -C $destdir || { echo "Error extracting $archive"; exit 1; }
	fi
done

#---------------------------------------------------------------------------------
# Add installed devkit to the path
#---------------------------------------------------------------------------------
export PATH=$PATH:$prefix/bin

#---------------------------------------------------------------------------------
# Build and install devkit components
#---------------------------------------------------------------------------------
cd $BUILDSCRIPTDIR

if [ -f $scriptdir/build-gcc.sh ]
then
	. $scriptdir/build-gcc.sh || { echo "Error building toolchain"; exit 1; }; cd $BUILDSCRIPTDIR;
fi

if [ -f $scriptdir/build-tools.sh ]
then
	. $scriptdir/build-tools.sh || { echo "Error building tools"; exit 1; }; cd $BUILDSCRIPTDIR;
fi

if [ -f $scriptdir/build-libs.sh ]
then
	. $scriptdir/build-libs.sh || { echo "Error building libraries"; exit 1; }; cd $BUILDSCRIPTDIR;
fi

#---------------------------------------------------------------------------------
# Strip binaries
#---------------------------------------------------------------------------------
for f in $prefix/bin/* \
		 $prefix/$target/bin/* \
		 $prefix/libexec/gcc/$target/$GCC_VER/*
do
	# exclude so for linux/osx, directories .la files, embedspu script & the gccbug text file
	if ! [[ "$f" == *.so || -d $f || "$f" == *.la || "$f" == *-embedspu || "$f" == *-gccbug ]]
	then
		strip $f
	fi
done

#---------------------------------------------------------------------------------
# Strip debug info from libraries
#---------------------------------------------------------------------------------
find $prefix/lib/gcc/$target -name *.a -exec $target-strip -d {} \;
find $prefix/$target -name *.a -exec $target-strip -d {} \;

#---------------------------------------------------------------------------------
# Clean up temporary files and source directories
#---------------------------------------------------------------------------------
#rm -fr $BUILDDIR
#rm -fr $SRCDIR
