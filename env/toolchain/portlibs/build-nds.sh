#!/bin/bash

#---------------------------------------------------------------------------------
# Set env variables
#---------------------------------------------------------------------------------
export DEVKITPRO=/opt/devkitpro
export DEVKITARM=${DEVKITPRO}/devkitARM
export PORTLIBS_ROOT=${DEVKITPRO}/portlibs
export PATH=${DEVKITPRO}/tools/bin:$DEVKITARM/bin:$PATH
export TOOL_PREFIX=arm-none-eabi-
export AR=${TOOL_PREFIX}gcc-ar
export RANLIB=${TOOL_PREFIX}gcc-ranlib

PORTLIBS_PREFIX=${PORTLIBS_ROOT}/nds
PATH=$PORTLIBS_PREFIX/bin:$PATH

export CFLAGS="-march=armv5te -mtune=arm946e-s -O2 -ffunction-sections -fdata-sections"
export CXXFLAGS="${CFLAGS}"
export CPPFLAGS="-D__NDS__ -DARM9 -I${PORTLIBS_PREFIX}/include -I${DEVKITPRO}/libnds/include"
export LDFLAGS="-L${PORTLIBS_PREFIX}/lib -L${DEVKITPRO}/libnds/lib"
export LIBS="-lnds9"

archives="bzip2-1.0.8.tar.gz
	zlib-1.2.11.tar.xz
	libpng-1.6.37.tar.xz
	freetype-2.9.tar.gz
	expat-2.2.5.tar.bz2"

BUILDSCRIPTDIR=$(pwd)
SRCDIR=$(pwd)/.download
mkdir -p $SRCDIR
cd $SRCDIR

for archive in $archives
do
	echo $archive
	if [ ! -f $archive ]
	then
		wget http://asscore.pl/download/devkitpro/$archive || { echo "Error: Failed to download $archive"; exit 1; }
	fi
done

cd $BUILDSCRIPTDIR
BUILDDIR=$(pwd)/.build_nds
mkdir -p $BUILDDIR
cd $BUILDDIR

for archive in $archives
do
	destdir=$(echo $archive | sed -r 's/\.[[:alnum:]]+\.[[:alnum:]]+$//' )
	if [ ! -d $destdir ]
	then
		echo "extracting $archive"
		tar xf "$SRCDIR/$archive"
	fi
done

#---------------------------------------------------------------------------------
cd $BUILDSCRIPTDIR
install -d "${PORTLIBS_PREFIX}/bin"
cp pkg-config.in "${PORTLIBS_PREFIX}/bin/arm-none-eabi-pkg-config"
sed 's/\$prefix/\/opt\/devkitpro\/portlibs\/nds/' -i "${PORTLIBS_PREFIX}/bin/arm-none-eabi-pkg-config"
chmod +x "${PORTLIBS_PREFIX}/bin/arm-none-eabi-pkg-config"

#---------------------------------------------------------------------------------
cd $BUILDDIR/bzip2-1.0.8
make CC=arm-none-eabi-gcc \
	AR=arm-none-eabi-ar \
	RANLIB=arm-none-eabi-ranlib \
	CPPFLAGS="${CPPFLAGS}" \
	CFLAGS="-D_FILE_OFFSET_BITS=64 -Winline ${CFLAGS}" libbz2.a \
	|| { echo "Error building bzip2"; exit 1; }

install -Dm 644 bzlib.h -t "${PORTLIBS_PREFIX}/include/" || { echo "Error installing bzip2"; exit 1; }
install -Dm 644 libbz2.a -t "${PORTLIBS_PREFIX}/lib" || { echo "Error installing bzip2"; exit 1; }

#---------------------------------------------------------------------------------
cd $BUILDDIR/zlib-1.2.11
CHOST=arm-none-eabi ./configure --prefix="${PORTLIBS_PREFIX}" --static
make libz.a || { echo "Error building zlib"; exit 1; }

make install || { echo "Error installing zlib"; exit 1; }

# libminizip
cd contrib/minizip
autoreconf --force --verbose --install
CFLAGS="${CFLAGS} -DUSE_FILE32API"
./configure --prefix="${PORTLIBS_PREFIX}" --host=arm-none-eabi \
	--disable-shared --enable-static \
	|| { echo "Error configuring minizip"; exit 1; }
make || { echo "Error building minizip"; exit 1; }

make install || { echo "Error installing minizip"; exit 1; }

#---------------------------------------------------------------------------------
cd $BUILDDIR/libpng-1.6.37
sed -i 's/^bin_PROGRAMS = .*//' Makefile.in
./configure --prefix="${PORTLIBS_PREFIX}" --host=arm-none-eabi \
	--disable-shared --enable-static \
	|| { echo "Error configuring libpng"; exit 1; }
make || { echo "Error building libpng"; exit 1; }

make LN_S=cp install || { echo "Error installing libpng"; exit 1; }

#---------------------------------------------------------------------------------
cd $BUILDDIR/freetype-2.9
./configure --prefix="${PORTLIBS_PREFIX}" --host=arm-none-eabi \
	--disable-shared --enable-static \
	--with-zlib \
	--with-bzip2 \
	--with-png \
	|| { echo "Error configuring freetype"; exit 1; }
make || { echo "Error building freetype"; exit 1; }

make install || { echo "Error installing freetype"; exit 1; }
# patch the config script to always output static libraries when not relying on pkg-config
sed 's/\$show_static/yes/' -i "${PORTLIBS_PREFIX}/bin/freetype-config"

#---------------------------------------------------------------------------------
cd $BUILDDIR/expat-2.2.5
./configure --prefix="${PORTLIBS_PREFIX}" --host=arm-none-eabi \
	--disable-shared --enable-static \
	|| { echo "Error configuring expat"; exit 1; }
make || { echo "Error building expat"; exit 1; }

make install || { echo "Error installing expat"; exit 1; }

#---------------------------------------------------------------------------------
# remove useless stuff
rm -rf $PORTLIBS_PREFIX/bin/xmlwf
rm -rf $PORTLIBS_PREFIX/share/doc
rm -rf $PORTLIBS_PREFIX/share/man
