#!/bin/bash

cd $BUILDDIR

#---------------------------------------------------------------------------------
# Install the rules files
#---------------------------------------------------------------------------------
mkdir -p rules
cd rules
tar -xvf $SRCDIR/devkitarm-rules-$DKARM_RULES_VER.tar.xz
make install

#---------------------------------------------------------------------------------
# Install and build the crt0 files
#---------------------------------------------------------------------------------
cd $BUILDDIR

mkdir -p crtls
cd crtls
tar -xvf $SRCDIR/devkitarm-crtls-$DKARM_CRTLS_VER.tar.xz
make install

cd $BUILDDIR/libgba-$LIBGBA_VER
make || { echo "Error building libgba"; exit 1; }
make install || { echo "Error installing libgba"; exit 1; }

cd $BUILDDIR/libtonc-$LIBTONC_VER
make || { echo "Error building libtonc"; exit 1; }
make install || { echo "Error installing libtonc"; exit 1; }

cd $BUILDDIR/libnds-$LIBNDS_VER
make || { echo "Error building libnds"; exit 1; }
make install || { echo "Error installing libnds"; exit 1; }

cd $BUILDDIR/dswifi-$DSWIFI_VER
make || { echo "Error building dswifi"; exit 1; }
make install || { echo "Error installing dswifi"; exit 1; }

cd $BUILDDIR/maxmod-$MAXMOD_VER
make || { echo "Error building maxmod"; exit 1; }
make install || { echo "Error installing maxmod"; exit 1; }

cd $BUILDDIR/default-arm7-$DEFAULT_ARM7_VER
make || { echo "Error building default-arm7"; exit 1; }
make install || { echo "Error installing default-arm7"; exit 1; }

cd $BUILDDIR/libfat-$LIBFAT_VER
make nds-install || { echo "Error building nds libfat"; exit 1; }
make gba-install || { echo "Error installing gba libfat"; exit 1; }

cd $BUILDDIR/libfilesystem-$FILESYSTEM_VER
make || { echo "Error building libfilesystem"; exit 1; }
make install || { echo "Error installing libfilesystem"; exit 1; }
