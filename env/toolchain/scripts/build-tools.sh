#!/bin/bash

cd $BUILDDIR

for archive in $hostarchives
do
	dir=$(echo $archive | sed -e 's/\(.*\)\.tar\.bz2/\1/' )
	cd $BUILDDIR/$dir
	if [ ! -f configured ]
	then
		CPPFLAGS="$cppflags $CPPFLAGS" ./configure --prefix=$toolsprefix $CROSS_PARAMS || { echo "Error configuring $archive"; exit 1; }
		touch configured
	fi
	if [ ! -f built ]
	then
		make || { echo "Error building $dir"; exit 1; }
		touch built
	fi
	if [ ! -f installed ]
	then
		make install || { echo "Error installing $dir"; exit 1; }
		touch installed
	fi
done
